﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

	public GameObject explosion;
	
	void OnCollisionEnter(Collision col)
    {
		GameObject e = Instantiate(explosion, this.transform.position, Quaternion.identity);
		if(col.gameObject.CompareTag("Player"))
		{
			col.gameObject.GetComponent<Drive>().Health = col.gameObject.GetComponent<Drive>().Health-20;
		}
		else if(col.gameObject.CompareTag("Enemy"))
		{
			Debug.Log("hit enemy");
			col.gameObject.GetComponent<TankAI>().HPUpdate();
		}
    	Destroy(e,1.5f);
    	Destroy(this.gameObject);
    }

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
