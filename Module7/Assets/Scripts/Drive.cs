﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Drive : MonoBehaviour {
    public GameObject bullet;
    public GameObject bulletRef;
 	public float speed = 10.0F;
    public float rotationSpeed = 100.0F;
    public int Health;
    public GameObject player;
    void Update() {
        float translation = Input.GetAxis("Vertical") * speed;
        float rotation = Input.GetAxis("Horizontal") * rotationSpeed;
        translation *= Time.deltaTime;
        rotation *= Time.deltaTime;
        transform.Translate(0, 0, translation);
        transform.Rotate(0, rotation, 0);
        if(Input.GetMouseButtonDown(0))
        {
            GameObject b = Instantiate(bullet, bulletRef.transform.position, bulletRef.transform.rotation);
            b.GetComponent<Rigidbody>().AddForce(bulletRef.transform.forward*500) ;
        }
        if(Health<=0)
        {
            Destroy(this.gameObject);
        }
	}
}
