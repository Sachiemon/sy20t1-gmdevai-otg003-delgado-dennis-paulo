﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NormalDriver : MonoBehaviour
{
    public Transform goal;
    public float speed = 0;
    public float acceleration = 2
        , minSpeed = 0, maxSpeed = 10,
        deceleration = 5;
    public float breakAngle = 20;
    public float rotationSpeed = 10;
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        Vector3 LookAtGoal = new Vector3(goal.transform.position.x,
           this.transform.position.y,
           goal.transform.position.z);
        Vector3 Direction = LookAtGoal - this.transform.position;
        this.transform.rotation = Quaternion.Slerp(this.transform.rotation,
           Quaternion.LookRotation(Direction), Time.deltaTime * rotationSpeed);
        if (Vector3.Angle(goal.forward, this.transform.forward) > breakAngle && speed > 3)
        {
            speed = Mathf.Clamp(speed - (deceleration * Time.deltaTime), minSpeed, maxSpeed);
        }
        else
        {
            speed = Mathf.Clamp(speed + (acceleration * Time.deltaTime), minSpeed, maxSpeed);
        }


        this.transform.Translate(0, 0, speed);
    }
}
