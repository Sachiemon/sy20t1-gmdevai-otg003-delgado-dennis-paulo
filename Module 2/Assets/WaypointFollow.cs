﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaypointFollow : MonoBehaviour
{
    public float speed = 10;
    float rotSpeed = 3;
    float accuracy = 1;
    public UnityStandardAssets.Utility.WaypointCircuit circuit;
    int CurrentWayPointIndex=0;

    void Start()
    {
        //Waypoints = GameObject.FindGameObjectsWithTag("Waypoint");
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if(circuit.Waypoints.Length==0)
        {
            return;
        }
        GameObject currentWaypoint = circuit.Waypoints[CurrentWayPointIndex].gameObject;

        Vector3 LookAtGoal = new Vector3(currentWaypoint.transform.position.x, 
            this.transform.position.y, 
            currentWaypoint.transform.position.z);
        Vector3 Direction = LookAtGoal - this.transform.position;
        if(Direction.magnitude<5.0f)
        {
            CurrentWayPointIndex++;
            if(CurrentWayPointIndex>=circuit.Waypoints.Length)
            {
                CurrentWayPointIndex = 0;
            }
        }
        this.transform.rotation = Quaternion.Slerp(this.transform.rotation,
            Quaternion.LookRotation(Direction), Time.deltaTime * rotSpeed);
        this.transform.Translate(0, 0, speed * Time.deltaTime);
    }
}
