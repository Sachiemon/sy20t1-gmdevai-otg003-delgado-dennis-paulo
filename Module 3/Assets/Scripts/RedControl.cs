﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RedControl : MonoBehaviour
{
    Transform goal;
    float speed = 5.0f;
    float accuracy = 1.0f;
    float rotSpeed = 2.0f;
    public GameObject wpManager;
    GameObject[] wps;
    GameObject currentNode;
    int currentWpIndex = 0;
    Graph graph;

    void Start()
    {
        wps = wpManager.GetComponent<WaypointManager>().waypoints;
        graph = wpManager.GetComponent<WaypointManager>().graph;
        currentNode = wps[0];

    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (graph.getPathLength() == 0 || currentWpIndex == graph.getPathLength())
        {
            return;
        }
        if (Vector3.Distance(graph.getPathPoint(currentWpIndex).transform.position, transform.position) < accuracy)
        {
            currentWpIndex++;
        }
        if (currentWpIndex < graph.getPathLength())
        {

            goal = graph.getPathPoint(currentWpIndex).transform;
            Vector3 LookatGoal = new Vector3(goal.position.x, transform.position.y, goal.position.z);
            Vector3 direction = LookatGoal - this.transform.position;
            this.transform.rotation = Quaternion.Slerp(this.transform.rotation, Quaternion.LookRotation(direction), Time.deltaTime *
                rotSpeed);
            this.transform.Translate(0, 0, speed * Time.deltaTime);
        }
        else
        {
            currentWpIndex = 0;
        }

        currentNode = graph.getPathPoint(currentWpIndex);
    }
    public void TwinMountains()
    {
        graph.AStar(currentNode, wps[0]);
        currentWpIndex = 0;
    }
    public void Barracks()
    {
        graph.AStar(currentNode, wps[1]);
        currentWpIndex = 0;
    }
    public void CommandPost()
    {
        graph.AStar(currentNode, wps[2]);
        currentWpIndex = 0;
    }
    public void Radar()
    {
        graph.AStar(currentNode, wps[3]);
        currentWpIndex = 0;
    }
    public void CommandCenter()
    {
        graph.AStar(currentNode, wps[4]);
        currentWpIndex = 0;
    }
    public void Middle()
    {
        graph.AStar(currentNode, wps[5]);
        currentWpIndex = 0;
    }
    public void OilRefineryPumps()
    {
        graph.AStar(currentNode, wps[6]);
        currentWpIndex = 0;
    }
    public void Tankers()
    {
        graph.AStar(currentNode, wps[7]);
        currentWpIndex = 0;
    }
}
