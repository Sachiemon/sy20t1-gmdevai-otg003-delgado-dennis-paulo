﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PetMovement : MonoBehaviour
{
    private Vector3 Direction;
    public float speed;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Movement();
    }
    public void Movement()
    {
        if(Input.GetKey(KeyCode.W))
        {
            Direction.z += 5;
        }
        else if (Input.GetKey(KeyCode.S))
        {
            Direction.z -= 5;
        }
        else if (Input.GetKey(KeyCode.D))
        {
            Direction.x += 5;
        }
        else if (Input.GetKey(KeyCode.A))
        {
            Direction.x -= 5;
        }
        transform.Translate(Direction.normalized * speed * Time.deltaTime);
    }
}
