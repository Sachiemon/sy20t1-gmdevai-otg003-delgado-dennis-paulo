﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public struct Link
{
    public enum direction { UNI, BI };
    public GameObject node1;
    public GameObject node2;
    public direction dir;
}
public class Waypoint : MonoBehaviour
{
    public Link[] links;
    public GameObject[] waypoints;
   
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
