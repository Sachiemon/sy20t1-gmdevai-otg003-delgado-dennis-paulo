﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Petfollower : MonoBehaviour
{
    public Transform goal;
    public float speed=5;
    public float rotationspeed = 5;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 direction = goal.position - this.transform.position;
        this.transform.rotation = Quaternion.Slerp(this.transform.rotation, Quaternion.LookRotation(direction), Time.deltaTime * rotationspeed);
        if (direction.magnitude>2)
        {
            
            transform.Translate(direction.normalized * speed * Time.deltaTime);
        }
    }
}
